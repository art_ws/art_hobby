package com.art.ball.common.helper;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;

import com.art.ball.config.ConfigurationFactory;

public class SpringAppHelper {

	public static SpringApplication createApp(Class<?>... sources) {
		return new SpringApplicationBuilder(sources)
				.properties(ConfigurationFactory.getConfiguration().getPropertyMap())
				.build();
	}

	private SpringAppHelper() {
	}

}
