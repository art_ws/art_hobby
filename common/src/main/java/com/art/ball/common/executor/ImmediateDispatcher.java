package com.art.ball.common.executor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ImmediateDispatcher<T> extends AbstractDispatcher<T> {

    private static final Logger LOGGER = LoggerFactory.getLogger(ImmediateDispatcher.class);

    @Override
    protected void runTask(Task<T> task) {
        LOGGER.info("Running task with id {}", task.getId());
        task.init();
        task.run();
        LOGGER.info("Ran task with id {}", task.getId());
    }

}
