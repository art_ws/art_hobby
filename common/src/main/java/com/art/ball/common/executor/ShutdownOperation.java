package com.art.ball.common.executor;

import java.util.concurrent.TimeUnit;

@FunctionalInterface
public interface ShutdownOperation {

    void awaitTimeout(long timeout, TimeUnit unit);
    
}
