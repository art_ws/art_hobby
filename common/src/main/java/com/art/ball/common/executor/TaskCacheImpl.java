package com.art.ball.common.executor;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

class TaskCacheImpl<T> implements TaskCache<T> {

    private final ConcurrentMap<T, Task<T>> cache = new ConcurrentHashMap<>();
    
    @Override
    public boolean put(Task<T> t) {
        return cache.put(t.getId(), t) == null;
    }

    @Override
    public Task<T> remove(Task<T> t) {
        return cache.remove(t.getId());
    }
    
}
