package com.art.ball.common.executor;

import java.util.concurrent.atomic.AtomicBoolean;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class AbstractDispatcher<T> implements Dispatcher<T> {

    private static final Logger LOGGER = LoggerFactory.getLogger(AbstractDispatcher.class);
    
    private static final ShutdownOperation EMPTY = (t, u) -> {};
    
    private final AtomicBoolean destroyed = new AtomicBoolean(false);

    protected AbstractDispatcher() {
    }
    
    @Override
    public void submit(Task<T> task) {
        if(!isDestroyed()) {
            LOGGER.info("Submitting task with id {}", task.getId());
            try {
                runTask(task);
            } catch(Exception e) {
                LOGGER.error("Exception occurred while executing task {}", task.getId(), e);
            }
            LOGGER.info("Submitted task with id {}", task.getId());
        } else {
            LOGGER.info("Dispatcher in destroyed state");
        }
    }
    
    protected abstract void runTask(Task<T> task);
    
    @Override
    public boolean isDestroyed() {
        return destroyed.get();
    }
    
    @Override
    public ShutdownOperation destroy() {
        if(destroyed.compareAndSet(false, true)) {
            return doDestroy();
        }
        return EMPTY;
    }
    
    protected ShutdownOperation doDestroy() {
        return EMPTY;
    }
    
}
