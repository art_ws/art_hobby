package com.art.ball.common.executor;

public interface Destroyable {

    boolean isDestroyed();
    
    ShutdownOperation destroy();
    
}
