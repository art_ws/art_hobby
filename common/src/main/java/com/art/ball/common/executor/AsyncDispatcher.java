package com.art.ball.common.executor;

import static java.util.Objects.requireNonNull;

import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AsyncDispatcher<T> extends AbstractDispatcher<T> {

    private static final Logger LOGGER = LoggerFactory.getLogger(AsyncDispatcher.class);
    
    private final ExecutorService executor;
    
    private final TaskCache<T> taskCache;
    
    @SuppressWarnings("unchecked")
    public AsyncDispatcher(ExecutorService executor, boolean dontAllowDuplicateTasks) {
        this.executor = requireNonNull(executor, "executor");
        this.taskCache = dontAllowDuplicateTasks ? new TaskCacheImpl<>() : new EmptyTaskCache<>();
        Optional.of(executor)
            .filter(e -> e instanceof ThreadPoolExecutor)
            .map(e -> (ThreadPoolExecutor) e)
            .ifPresent(e -> e.setRejectedExecutionHandler((r, exec) -> ((Task<T>) r).onReject()));
    }
    
    @Override
    protected void runTask(Task<T> task) {
        if(taskCache.put(task)) {
            TaskDelegate<T> tw = new TaskDelegate<>(taskCache, task);
            tw.init();
            executor.execute(tw);
        } else {
            LOGGER.info("Duplicate task id {}", task.getId());
        }
    }

    @Override
    protected ShutdownOperation doDestroy() {
        return new ShutdownOp(executor);
    }
    
    private static class TaskDelegate<T> implements Task<T> {
        
        private final TaskCache<T> cache;
        
        private final Task<T> task;
        
        TaskDelegate(TaskCache<T> cache, Task<T> task) {
            this.cache = cache;
            this.task = task;
        }
        
        @Override
        public T getId() {
            return task.getId();
        }
        
        @Override
        public Map<String, String> getParams() {
            return task.getParams();
        }
        
        @Override
        public void init() {
            task.init();
        }
        
        @Override
        public void run() {
            executeAndRemoveFromCache(task::run);
        }
        
        @Override
        public void onReject() {
            executeAndRemoveFromCache(task::onReject);
        }
        
        private void executeAndRemoveFromCache(Runnable r) {
            try {
                r.run();
            } finally {
                cache.remove(task);
            }
        }
        
    }
    
    private static class EmptyTaskCache<T> implements TaskCache<T> {
        
        @Override
        public boolean put(Task<T> t) {
            return true;
        }
        
        @Override
        public Task<T> remove(Task<T> t) {
            return t;
        }
        
    }
    
    public static class ShutdownOp implements ShutdownOperation {

        private final ExecutorService exec;
        
        public ShutdownOp(ExecutorService exec) {
            this.exec = exec;
            exec.shutdownNow();
        }
        
        @Override
        public void awaitTimeout(long timeout, TimeUnit unit) {
            try {
                exec.awaitTermination(timeout, unit);
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
            }
        }

    }
    
}
