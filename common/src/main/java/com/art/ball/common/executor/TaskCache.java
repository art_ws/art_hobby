package com.art.ball.common.executor;

interface TaskCache<T> {

    boolean put(Task<T> t);

    Task<T> remove(Task<T> t);

}
