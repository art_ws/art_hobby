package com.art.ball.common.executor;

import java.util.Map;

public interface Task<T> extends Runnable {

    T getId();
    
    Map<String, String> getParams();

    void init();
    
    @Override
    void run();
    
    void onReject();
    
}
