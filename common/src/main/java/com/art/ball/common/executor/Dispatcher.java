package com.art.ball.common.executor;

public interface Dispatcher<T> extends Destroyable {

    void submit(Task<T> task);

}
