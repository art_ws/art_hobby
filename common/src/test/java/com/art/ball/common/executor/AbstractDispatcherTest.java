package com.art.ball.common.executor;

import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Collections;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.stream.Stream;

import org.awaitility.Awaitility;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.Mockito;

final class AbstractDispatcherTest {

    @SuppressWarnings("unchecked")
    @ParameterizedTest
    @MethodSource("argsTestExceptionInInit")
    final void testExceptionInInit(AbstractDispatcher<String> dispatcher) {
        Task<String> task = Mockito.mock(Task.class);
        Mockito.when(task.getId()).thenReturn("1");
        Mockito.when(task.getParams()).thenReturn(Collections.emptyMap());
        Mockito.doThrow(RuntimeException.class).when(task).init();
        
        dispatcher.submit(task);
        Awaitility.await()
            .pollInterval(10, TimeUnit.MILLISECONDS)
            .atMost(50, TimeUnit.MILLISECONDS);
        
        Mockito.verify(task).init();
        Mockito.verify(task, Mockito.never()).run();
        Mockito.verify(task, Mockito.never()).onReject();
        
        dispatcher.destroy().awaitTimeout(1, TimeUnit.SECONDS);
        assertTrue(dispatcher.isDestroyed());
    }

    private static Stream<Arguments> argsTestExceptionInInit() {
        return Stream.of(
                Arguments.of(new ImmediateDispatcher<String>()),
                Arguments.of(new AsyncDispatcher<String>(Executors.newSingleThreadExecutor(), false)),
                Arguments.of(new AsyncDispatcher<String>(Executors.newSingleThreadExecutor(), true))
             );
    }

}
