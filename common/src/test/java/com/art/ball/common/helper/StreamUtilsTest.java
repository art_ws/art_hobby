package com.art.ball.common.helper;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.function.Function;
import java.util.function.UnaryOperator;
import java.util.stream.Stream;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

final class StreamUtilsTest {

    @ParameterizedTest
    @MethodSource("argsTestDistinctByKey")
    final <T> void testDistinctByKey(Collection<T> coll, Function<? super T, ?> keyExtractor, int expectedSize) {
        long cnt = coll.stream().filter(StreamUtils.distinctByKey(keyExtractor)).count();
        assertEquals(expectedSize, cnt);
    }

    private static Stream<Arguments> argsTestDistinctByKey() {
        List<Person> coll = Arrays.asList(
                    new Person(1, "Abc"),
                    new Person(2, "Pqr"),
                    new Person(3, "Xyz"),
                    new Person(1, "Abc"),
                    new Person(4, "Abc")
                );
        
        return Stream.of(
                Arguments.of(coll, (UnaryOperator<Person>) p -> p, 5),
                Arguments.of(coll, (Function<Person, Integer>) Person::getId, 4),
                Arguments.of(coll, (Function<Person, String>) Person::getName, 3)
             );
    }

}
