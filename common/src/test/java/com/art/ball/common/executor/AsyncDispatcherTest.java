package com.art.ball.common.executor;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import org.awaitility.Awaitility;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

final class AsyncDispatcherTest {

    @ParameterizedTest
    @ValueSource(booleans = { true, false })
    final void testSubmitAndDestroy(boolean dontAllowDuplicateTasks) {
        AsyncDispatcher<String> dispatcher = new AsyncDispatcher<>(Executors.newSingleThreadExecutor(),
                dontAllowDuplicateTasks);

        FakeTask task = new FakeTask("123", 500);
        
        assertFalse(task.isInitialized());
        assertFalse(task.isExecuted());
        assertFalse(task.isInterrupted());

        dispatcher.submit(task);
        Awaitility.await()
            .pollInterval(20, TimeUnit.MILLISECONDS)
            .atMost(50, TimeUnit.MILLISECONDS)
            .until(() -> task.isExecuted());
        assertTrue(task.isInitialized());
        assertTrue(task.isExecuted());
        assertFalse(task.isInterrupted());
        
        assertFalse(dispatcher.isDestroyed());
        dispatcher.destroy().awaitTimeout(10, TimeUnit.SECONDS);
        Awaitility.await()
            .pollInterval(20, TimeUnit.MILLISECONDS)
            .atMost(50, TimeUnit.MILLISECONDS)
            .until(() -> task.isInterrupted());
        assertTrue(task.isInterrupted());
        assertTrue(dispatcher.isDestroyed());
    }
    
    @ParameterizedTest
    @ValueSource(booleans = { true, false })
    final void testDuplicateTaskBehaviour(boolean dontAllowDuplicateTasks) {
        AsyncDispatcher<String> dispatcher = new AsyncDispatcher<>(Executors.newSingleThreadExecutor(),
                dontAllowDuplicateTasks);

        FakeTask task1 = new FakeTask("123", 10);
        FakeTask task2 = new FakeTask("123", 10);
        
        assertFalse(task1.isInitialized());
        assertFalse(task1.isExecuted());
        assertFalse(task1.isInterrupted());
        assertFalse(task1.isRejected());
        assertFalse(task2.isInitialized());
        assertFalse(task2.isExecuted());
        assertFalse(task2.isInterrupted());
        assertFalse(task2.isRejected());
        
        dispatcher.submit(task1);
        dispatcher.submit(task2);
        Awaitility.await()
            .pollInterval(10, TimeUnit.MILLISECONDS)
            .atMost(50, TimeUnit.MILLISECONDS)
            .until(() -> task1.isExecuted());
        assertTrue(task1.isInitialized());
        assertTrue(task1.isExecuted());
        assertFalse(task1.isRejected());
        assertFalse(task1.isInterrupted());
        
        if(dontAllowDuplicateTasks) {
            assertFalse(task2.isInitialized());
            assertFalse(task2.isExecuted());
        } else {
            assertTrue(task2.isInitialized());
            assertTrue(task2.isExecuted());
        }
        assertFalse(task2.isRejected());
        assertFalse(task2.isInterrupted());
        
        dispatcher.destroy().awaitTimeout(1, TimeUnit.SECONDS);
        assertTrue(dispatcher.isDestroyed());
    }
    
    @ParameterizedTest
    @ValueSource(booleans = { true, false })
    final void testOnReject(boolean dontAllowDuplicateTasks) {
        ExecutorService exec = new ThreadPoolExecutor(1, 1, 10, TimeUnit.SECONDS, new LinkedBlockingQueue<>(1));
        AsyncDispatcher<String> dispatcher = new AsyncDispatcher<>(exec, dontAllowDuplicateTasks);

        FakeTask task1 = new FakeTask("123", 100);
        FakeTask task2 = new FakeTask("456", 10);
        FakeTask task3 = new FakeTask("789", 10);
        
        assertFalse(task1.isInitialized());
        assertFalse(task1.isExecuted());
        assertFalse(task1.isInterrupted());
        assertFalse(task1.isRejected());
        assertFalse(task2.isInitialized());
        assertFalse(task2.isExecuted());
        assertFalse(task2.isInterrupted());
        assertFalse(task2.isRejected());
        assertFalse(task3.isInitialized());
        assertFalse(task3.isExecuted());
        assertFalse(task3.isInterrupted());
        assertFalse(task3.isRejected());
        
        dispatcher.submit(task1);
        dispatcher.submit(task2);
        dispatcher.submit(task3);
        Awaitility.await()
            .pollInterval(20, TimeUnit.MILLISECONDS)
            .atMost(50, TimeUnit.MILLISECONDS)
            .until(() -> task3.isRejected());
        assertTrue(task1.isInitialized());
        assertTrue(task1.isExecuted());
        assertFalse(task1.isRejected());
        assertFalse(task1.isInterrupted());
        
        assertTrue(task2.isInitialized());
        assertFalse(task2.isExecuted());
        assertFalse(task2.isRejected());
        assertFalse(task2.isInterrupted());

        assertTrue(task3.isInitialized());
        assertFalse(task3.isExecuted());
        assertTrue(task3.isRejected());
        assertFalse(task3.isInterrupted());
        
        dispatcher.destroy().awaitTimeout(1, TimeUnit.SECONDS);
        assertTrue(dispatcher.isDestroyed());
    }

}
