package com.art.ball.common.executor;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.time.Duration;
import java.util.concurrent.TimeUnit;

import org.awaitility.Awaitility;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

final class ImmediateDispatcherTest {

    @SuppressWarnings("unchecked")
    @Test
    final void testSubmitAndDestroy() {
        ImmediateDispatcher<String> dispatcher = new ImmediateDispatcher<>();

        Task<String> task = Mockito.mock(Task.class);
        dispatcher.submit(task);

        Mockito.verify(task).init();
        Mockito.verify(task).run();
        Mockito.verify(task, Mockito.never()).onReject();

        assertFalse(dispatcher.isDestroyed());
        dispatcher.destroy().awaitTimeout(10, TimeUnit.SECONDS);
        Awaitility.await()
            .pollInterval(20, TimeUnit.MILLISECONDS)
            .atMost(Duration.ofMillis(50))
            .until(() -> dispatcher.isDestroyed());
        
        assertTrue(dispatcher.isDestroyed());
        Mockito.verify(task, Mockito.never()).onReject();
    }

}
