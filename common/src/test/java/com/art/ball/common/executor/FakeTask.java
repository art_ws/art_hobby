package com.art.ball.common.executor;

import java.util.Collections;
import java.util.Map;

public class FakeTask implements Task<String> {

    private final String id;

    private final long sleepInMillis;

    private volatile boolean initialized;
    
    private volatile boolean executed;

    private volatile boolean interrupted;
    
    private volatile boolean rejected;

    FakeTask(String id, long sleepInMillis) {
        this.id = id;
        this.sleepInMillis = sleepInMillis;
    }

    @Override
    public void init() {
        initialized = true;
    }
    
    @Override
    public String getId() {
        return id;
    }

    @Override
    public Map<String, String> getParams() {
        return Collections.emptyMap();
    }

    public boolean isInitialized() {
        return initialized;
    }
    
    public boolean isExecuted() {
        return executed;
    }

    public boolean isInterrupted() {
        return interrupted;
    }

    public boolean isRejected() {
        return rejected;
    }
    
    @Override
    public void run() {
        executed = true;
        try {
            Thread.sleep(sleepInMillis);
        } catch (InterruptedException e) {
            interrupted = true;
            Thread.currentThread().interrupt();
        }
    }

    @Override
    public void onReject() {
        rejected = true;
    }
    
}
