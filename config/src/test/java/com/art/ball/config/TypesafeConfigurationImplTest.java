package com.art.ball.config;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.File;
import java.time.DayOfWeek;
import java.util.Arrays;
import java.util.concurrent.TimeUnit;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import com.art.ball.config.Configuration;
import com.art.ball.config.ConfigurationException;
import com.art.ball.config.TypesafeConfigurationImpl;
import com.typesafe.config.ConfigFactory;

final class TypesafeConfigurationImplTest {

	private static final double DELTA = 0.000000000000001d;

	private static Configuration config;

	@BeforeAll
	private static void beforeAll() {
		config = new TypesafeConfigurationImpl(
				ConfigFactory.parseFile(new File("src/test/resources/test_config.conf")).resolve());
	}

	@Test
	final void testTypesafeImpl() {
		assertTrue(config.getBoolean("bool1"));
		assertFalse(config.getBoolean("bool2"));

		assertTrue(config.getBoolean("bool1", false));
		assertTrue(config.getBoolean("bool1", true));
		assertFalse(config.getBoolean("bool2", false));
		assertFalse(config.getBoolean("bool2", true));

		assertEquals((int) 123, config.getInt("prop.int1"));
		assertEquals(123L, config.getLong("prop.int1"));
		assertEquals(555L, config.getLong("prop.long1"));
		assertEquals((int) 555, config.getInt("prop.long1"));
		assertEquals(34.66d, config.getDouble("prop.double1"), DELTA);
		assertEquals("hello", config.getString("prop.string1"));
		assertEquals(DayOfWeek.FRIDAY, config.getEnum(DayOfWeek.class, "prop.enum1"));

		assertEquals((int) 123, config.getInt("prop.int1", 0));
		assertEquals(123L, config.getLong("prop.int1", 0));
		assertEquals(555L, config.getLong("prop.long1", 0));
		assertEquals((int) 555, config.getInt("prop.long1", 0));
		assertEquals(34.66d, config.getDouble("prop.double1", 0), DELTA);
		assertEquals("hello", config.getString("prop.string1", ""));
		assertEquals(DayOfWeek.FRIDAY, config.getEnum(DayOfWeek.class, "prop.enum1", DayOfWeek.SUNDAY));

		assertEquals(Arrays.asList((int) 40, (int) 33, (int) 1209), config.getInts("nested.prop.intList"));
		assertEquals(Arrays.asList(40L, 33L, 1209L), config.getLongs("nested.prop.intList"));
		assertEquals(Arrays.asList(99999L, 87L), config.getLongs("nested.prop.longList"));
		assertEquals(Arrays.asList((int) 99999, (int) 87), config.getInts("nested.prop.longList"));
		assertEquals(Arrays.asList(99.01d, 67.34d, 2d), config.getDoubles("nested.prop.doubleList"));
		assertEquals(Arrays.asList("abc", "pqr"), config.getStrings("nested.prop.stringList"));
		assertEquals(Arrays.asList(true, false, true), config.getBooleans("nested.prop.boolList"));
		assertEquals(Arrays.asList(DayOfWeek.MONDAY, DayOfWeek.SATURDAY),
				config.getEnums(DayOfWeek.class, "nested.prop.enumList"));

		assertEquals(Arrays.asList((int) 40, (int) 33, (int) 1209), config.getIntsOrEmpty("nested.prop.intList"));
		assertEquals(Arrays.asList(40L, 33L, 1209L), config.getLongsOrEmpty("nested.prop.intList"));
		assertEquals(Arrays.asList(99999L, 87L), config.getLongsOrEmpty("nested.prop.longList"));
		assertEquals(Arrays.asList((int) 99999, (int) 87), config.getIntsOrEmpty("nested.prop.longList"));
		assertEquals(Arrays.asList(99.01d, 67.34d, 2d), config.getDoublesOrEmpty("nested.prop.doubleList"));
		assertEquals(Arrays.asList("abc", "pqr"), config.getStringsOrEmpty("nested.prop.stringList"));
		assertEquals(Arrays.asList(true, false, true), config.getBooleansOrEmpty("nested.prop.boolList"));
		assertEquals(Arrays.asList(DayOfWeek.MONDAY, DayOfWeek.SATURDAY),
				config.getEnumsOrEmpty(DayOfWeek.class, "nested.prop.enumList"));

		assertTrue(config.contains("bool1"));
		assertTrue(config.contains("nested"));
		assertFalse(config.contains("bool3"));

		Configuration child = config.getConfiguration("nested");
		assertEquals(Arrays.asList((int) 40, (int) 33, (int) 1209), child.getInts("prop.intList"));
		assertEquals(Arrays.asList(40L, 33L, 1209L), child.getLongs("prop.intList"));
		assertEquals(Arrays.asList(99999L, 87L), child.getLongs("prop.longList"));
		assertEquals(Arrays.asList((int) 99999, (int) 87), child.getInts("prop.longList"));
		assertEquals(Arrays.asList(99.01d, 67.34d, 2d), child.getDoubles("prop.doubleList"));
		assertEquals(Arrays.asList("abc", "pqr"), child.getStrings("prop.stringList"));
		assertEquals(Arrays.asList(true, false, true), child.getBooleans("prop.boolList"));
		assertEquals(Arrays.asList(DayOfWeek.MONDAY, DayOfWeek.SATURDAY),
				child.getEnumsOrEmpty(DayOfWeek.class, "prop.enumList"));
	}

	@Test
	final void testDefaults() {
		assertFalse(config.contains("doesntexist"));

		assertTrue(config.getBoolean("doesntexist", true));
		assertFalse(config.getBoolean("doesntexist", false));

		assertEquals((int) 10, config.getInt("doesntexist", 10));
		assertEquals(101L, config.getLong("doesntexist", 101L));
		assertEquals(10.9, config.getDouble("doesntexist", 10.9), DELTA);
		assertEquals("what to do?", config.getString("doesntexist", "what to do?"));
		assertEquals(TimeUnit.SECONDS, config.getEnum(TimeUnit.class, "doesntexist", TimeUnit.SECONDS));

		assertTrue(config.getIntsOrEmpty("doesntexist").isEmpty());
		assertTrue(config.getLongsOrEmpty("doesntexist").isEmpty());
		assertTrue(config.getDoublesOrEmpty("doesntexist").isEmpty());
		assertTrue(config.getStringsOrEmpty("doesntexist").isEmpty());
		assertTrue(config.getBooleansOrEmpty("doesntexist").isEmpty());
		assertTrue(config.getEnumsOrEmpty(TimeUnit.class, "doesntexist").isEmpty());
	}

	@Test
	final void testMissingException() {
		assertFalse(config.contains("doesntexist"));

		Throwable th = assertThrows(ConfigurationException.class, () -> config.getBoolean("doesntexist"));
		assertEquals("doesntexist", th.getMessage());

		Throwable th2 = assertThrows(ConfigurationException.class, () -> config.getInt("doesntexist"));
		assertEquals("doesntexist", th2.getMessage());

		Throwable th3 = assertThrows(ConfigurationException.class, () -> config.getLong("doesntexist"));
		assertEquals("doesntexist", th3.getMessage());

		Throwable th4 = assertThrows(ConfigurationException.class, () -> config.getDouble("doesntexist"));
		assertEquals("doesntexist", th4.getMessage());

		Throwable th5 = assertThrows(ConfigurationException.class,
				() -> config.getEnum(DayOfWeek.class, "doesntexist"));
		assertEquals("doesntexist", th5.getMessage());

		Throwable th6 = assertThrows(ConfigurationException.class, () -> config.getBooleans("doesntexist"));
		assertEquals("doesntexist", th6.getMessage());

		Throwable th7 = assertThrows(ConfigurationException.class, () -> config.getInts("doesntexist"));
		assertEquals("doesntexist", th7.getMessage());

		Throwable th8 = assertThrows(ConfigurationException.class, () -> config.getLongs("doesntexist"));
		assertEquals("doesntexist", th8.getMessage());

		Throwable th9 = assertThrows(ConfigurationException.class, () -> config.getDoubles("doesntexist"));
		assertEquals("doesntexist", th9.getMessage());

		Throwable th10 = assertThrows(ConfigurationException.class,
				() -> config.getEnums(DayOfWeek.class, "doesntexist"));
		assertEquals("doesntexist", th10.getMessage());
	}

	@Test
	final void testWrongTypeException() {
		Throwable th = assertThrows(ConfigurationException.class, () -> config.getInt("bool1"));
		assertEquals("bool1", th.getMessage());

		Throwable th2 = assertThrows(ConfigurationException.class, () -> config.getBoolean("prop.int1"));
		assertEquals("prop.int1", th2.getMessage());

		Throwable th3 = assertThrows(ConfigurationException.class, () -> config.getLong("prop.string1"));
		assertEquals("prop.string1", th3.getMessage());

		Throwable th4 = assertThrows(ConfigurationException.class, () -> config.getEnum(DayOfWeek.class, "prop.long1"));
		assertEquals("prop.long1", th4.getMessage());

		Throwable th5 = assertThrows(ConfigurationException.class, () -> config.getBooleans("nested.prop.longList"));
		assertEquals("nested.prop.longList", th5.getMessage());

		Throwable th6 = assertThrows(ConfigurationException.class, () -> config.getInts("nested.prop.boolList"));
		assertEquals("nested.prop.boolList", th6.getMessage());

		Throwable th7 = assertThrows(ConfigurationException.class, () -> config.getDoubles("nested.prop.stringList"));
		assertEquals("nested.prop.stringList", th7.getMessage());

		Throwable th8 = assertThrows(ConfigurationException.class,
				() -> config.getEnums(DayOfWeek.class, "prop.long1"));
		assertEquals("prop.long1", th8.getMessage());

		Throwable th9 = assertThrows(ConfigurationException.class, () -> config.getInt("bool1", 1));
		assertEquals("bool1", th9.getMessage());

		Throwable th10 = assertThrows(ConfigurationException.class, () -> config.getBoolean("prop.int1", true));
		assertEquals("prop.int1", th10.getMessage());

		Throwable th11 = assertThrows(ConfigurationException.class, () -> config.getLong("prop.string1", 100L));
		assertEquals("prop.string1", th11.getMessage());

		Throwable th12 = assertThrows(ConfigurationException.class,
				() -> config.getEnum(DayOfWeek.class, "prop.long1", DayOfWeek.TUESDAY));
		assertEquals("prop.long1", th12.getMessage());

		Throwable th13 = assertThrows(ConfigurationException.class,
				() -> config.getBooleansOrEmpty("nested.prop.longList"));
		assertEquals("nested.prop.longList", th13.getMessage());

		Throwable th14 = assertThrows(ConfigurationException.class,
				() -> config.getIntsOrEmpty("nested.prop.boolList"));
		assertEquals("nested.prop.boolList", th14.getMessage());

		Throwable th15 = assertThrows(ConfigurationException.class,
				() -> config.getDoublesOrEmpty("nested.prop.stringList"));
		assertEquals("nested.prop.stringList", th15.getMessage());

		Throwable th16 = assertThrows(ConfigurationException.class,
				() -> config.getEnumsOrEmpty(DayOfWeek.class, "prop.long1"));
		assertEquals("prop.long1", th16.getMessage());
	}

}
