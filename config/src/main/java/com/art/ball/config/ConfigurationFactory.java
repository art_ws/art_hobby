package com.art.ball.config;

import com.typesafe.config.ConfigFactory;

public class ConfigurationFactory {

	private static final Configuration CONFIG = new TypesafeConfigurationImpl(ConfigFactory.load().resolve());

	public static Configuration getConfiguration() {
		return CONFIG;
	}

	private ConfigurationFactory() {
	}

}
