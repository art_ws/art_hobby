package com.art.ball.config;

import static java.util.Objects.requireNonNull;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.function.Function;
import java.util.stream.Collectors;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigException;

class TypesafeConfigurationImpl implements Configuration {

	private final Config typesafe;

	TypesafeConfigurationImpl(Config typesafe) {
		this.typesafe = requireNonNull(typesafe, "typesafe");
	}

	@Override
	public boolean getBoolean(String key) {
		try {
			return typesafe.getBoolean(key);
		} catch (ConfigException.Missing | ConfigException.WrongType | ConfigException.BadValue e) {
			throw new ConfigurationException(key, e);
		}
	}

	@Override
	public int getInt(String key) {
		try {
			return typesafe.getInt(key);
		} catch (ConfigException.Missing | ConfigException.WrongType | ConfigException.BadValue e) {
			throw new ConfigurationException(key, e);
		}
	}

	@Override
	public long getLong(String key) {
		try {
			return typesafe.getLong(key);
		} catch (ConfigException.Missing | ConfigException.WrongType | ConfigException.BadValue e) {
			throw new ConfigurationException(key, e);
		}
	}

	@Override
	public double getDouble(String key) {
		try {
			return typesafe.getDouble(key);
		} catch (ConfigException.Missing | ConfigException.WrongType | ConfigException.BadValue e) {
			throw new ConfigurationException(key, e);
		}
	}

	@Override
	public String getString(String key) {
		try {
			return typesafe.getString(key);
		} catch (ConfigException.Missing | ConfigException.WrongType | ConfigException.BadValue e) {
			throw new ConfigurationException(key, e);
		}
	}

	@Override
	public <T extends Enum<T>> T getEnum(Class<T> type, String key) {
		try {
			return typesafe.getEnum(type, key);
		} catch (ConfigException.Missing | ConfigException.WrongType | ConfigException.BadValue e) {
			throw new ConfigurationException(key, e);
		}
	}

	@Override
	public boolean getBoolean(String key, boolean def) {
		try {
			return typesafe.getBoolean(key);
		} catch (ConfigException.Missing e) {
			return def;
		} catch (ConfigException.WrongType | ConfigException.BadValue e) {
			throw new ConfigurationException(key, e);
		}
	}

	@Override
	public int getInt(String key, int def) {
		try {
			return typesafe.getInt(key);
		} catch (ConfigException.Missing e) {
			return def;
		} catch (ConfigException.WrongType | ConfigException.BadValue e) {
			throw new ConfigurationException(key, e);
		}
	}

	@Override
	public long getLong(String key, long def) {
		try {
			return typesafe.getLong(key);
		} catch (ConfigException.Missing e) {
			return def;
		} catch (ConfigException.WrongType | ConfigException.BadValue e) {
			throw new ConfigurationException(key, e);
		}
	}

	@Override
	public double getDouble(String key, double def) {
		try {
			return typesafe.getDouble(key);
		} catch (ConfigException.Missing e) {
			return def;
		} catch (ConfigException.WrongType | ConfigException.BadValue e) {
			throw new ConfigurationException(key, e);
		}
	}

	@Override
	public String getString(String key, String def) {
		try {
			return typesafe.getString(key);
		} catch (ConfigException.Missing e) {
			return def;
		} catch (ConfigException.WrongType | ConfigException.BadValue e) {
			throw new ConfigurationException(key, e);
		}
	}

	@Override
	public <T extends Enum<T>> T getEnum(Class<T> type, String key, T def) {
		try {
			return typesafe.getEnum(type, key);
		} catch (ConfigException.Missing e) {
			return def;
		} catch (ConfigException.WrongType | ConfigException.BadValue e) {
			throw new ConfigurationException(key, e);
		}
	}

	private <T> List<T> getLists(String key, Function<String, List<T>> func) {
		try {
			return func.apply(key);
		} catch (ConfigException.Missing | ConfigException.WrongType | ConfigException.BadValue e) {
			throw new ConfigurationException(key, e);
		}
	}

	@Override
	public List<Boolean> getBooleans(String key) {
		return getLists(key, typesafe::getBooleanList);
	}

	@Override
	public List<Integer> getInts(String key) {
		return getLists(key, typesafe::getIntList);
	}

	@Override
	public List<Long> getLongs(String key) {
		return getLists(key, typesafe::getLongList);
	}

	@Override
	public List<Double> getDoubles(String key) {
		return getLists(key, typesafe::getDoubleList);
	}

	@Override
	public List<String> getStrings(String key) {
		return getLists(key, typesafe::getStringList);
	}

	@Override
	public <T extends Enum<T>> List<T> getEnums(Class<T> type, String key) {
		return getLists(key, k -> typesafe.getEnumList(type, k));
	}

	private <T> List<T> getListsOrEmpty(String key, Function<String, List<T>> func) {
		try {
			return func.apply(key);
		} catch (ConfigException.Missing e) {
			return Collections.emptyList();
		} catch (ConfigException.WrongType | ConfigException.BadValue e) {
			throw new ConfigurationException(key, e);
		}
	}

	@Override
	public List<Boolean> getBooleansOrEmpty(String key) {
		return getListsOrEmpty(key, typesafe::getBooleanList);
	}

	@Override
	public List<Integer> getIntsOrEmpty(String key) {
		return getListsOrEmpty(key, typesafe::getIntList);
	}

	@Override
	public List<Long> getLongsOrEmpty(String key) {
		return getListsOrEmpty(key, typesafe::getLongList);
	}

	@Override
	public List<Double> getDoublesOrEmpty(String key) {
		return getListsOrEmpty(key, typesafe::getDoubleList);
	}

	@Override
	public List<String> getStringsOrEmpty(String key) {
		return getListsOrEmpty(key, typesafe::getStringList);
	}

	@Override
	public <T extends Enum<T>> List<T> getEnumsOrEmpty(Class<T> type, String key) {
		return getListsOrEmpty(key, k -> typesafe.getEnumList(type, k));
	}

	@Override
	public boolean contains(String key) {
		return typesafe.hasPath(key);
	}

	@Override
	public Configuration getConfiguration(String key) {
		try {
			return new TypesafeConfigurationImpl(typesafe.getConfig(key));
		} catch (ConfigException.Missing | ConfigException.WrongType | ConfigException.BadValue e) {
			throw new ConfigurationException(key, e);
		}
	}

	@Override
	public Map<String, Object> getPropertyMap() {
		return typesafe.entrySet().stream()
				.collect(Collectors.toMap(Entry::getKey, e -> this.getString(e.getKey())));
	}
	
}
