package com.art.ball.config;

import java.util.List;
import java.util.Map;

public interface Configuration {

	boolean getBoolean(String key);

	int getInt(String key);

	long getLong(String key);

	double getDouble(String key);

	String getString(String key);

	<T extends Enum<T>> T getEnum(Class<T> enumType, String key);

	boolean getBoolean(String key, boolean def);

	int getInt(String key, int def);

	long getLong(String key, long def);

	double getDouble(String key, double def);

	String getString(String key, String def);

	<T extends Enum<T>> T getEnum(Class<T> enumType, String key, T def);

	List<Boolean> getBooleans(String key);

	List<Integer> getInts(String key);

	List<Long> getLongs(String key);

	List<Double> getDoubles(String key);

	List<String> getStrings(String key);

	<T extends Enum<T>> List<T> getEnums(Class<T> enumType, String key);

	List<Boolean> getBooleansOrEmpty(String key);

	List<Integer> getIntsOrEmpty(String key);

	List<Long> getLongsOrEmpty(String key);

	List<Double> getDoublesOrEmpty(String key);

	List<String> getStringsOrEmpty(String key);

	<T extends Enum<T>> List<T> getEnumsOrEmpty(Class<T> enumType, String key);

	boolean contains(String key);

	Configuration getConfiguration(String key);

	Map<String, Object> getPropertyMap();
	
}
