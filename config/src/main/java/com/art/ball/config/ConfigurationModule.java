package com.art.ball.config;

import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

@Component
class ConfigurationModule {

	@Bean
	Configuration configuration() {
		return ConfigurationFactory.getConfiguration();
	}

}
