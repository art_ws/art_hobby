package com.art.ball.executer.engine;

public interface FrequencySetting {

    StartOperation atFixedRate(long period);
    
    StartOperation withFixedDelay(long delay);
    
}
