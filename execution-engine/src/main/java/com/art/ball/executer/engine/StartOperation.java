package com.art.ball.executer.engine;

public interface StartOperation {

    void go();
    
}
