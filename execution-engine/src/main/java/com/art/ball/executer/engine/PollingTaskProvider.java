package com.art.ball.executer.engine;

import static java.util.Objects.requireNonNull;

import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.function.Supplier;

import com.art.ball.common.executor.AsyncDispatcher.ShutdownOp;
import com.art.ball.common.executor.ShutdownOperation;

public class PollingTaskProvider<T> extends AbstractTaskProvider<T> {

    private final ScheduledExecutorService scheduler;
    
    private final TaskReader<T> reader;
    
    public PollingTaskProvider(ScheduledExecutorService scheduler, TaskReader<T> reader) {
        this.scheduler = requireNonNull(scheduler, "scheduler");
        this.reader = requireNonNull(reader, "reader");
    }
    
    @Override
    public BasicSetting start() {
        return new Init(() -> fireTasks(reader.read()), scheduler);
    }

    @Override
    public ShutdownOperation doDestroy() {
        return new ShutdownOp(scheduler);
    }

    private static class Init implements BasicSetting {

        private final Runnable task;
        
        private final ScheduledExecutorService scheduler;
        
        Init(Runnable task, ScheduledExecutorService scheduler) {
            this.task = task;
            this.scheduler = scheduler;
        }

        @Override
        public void go() {
            withInitialDelay(0, TimeUnit.SECONDS)
                .withFixedDelay(1)
                .go();
        }
        
        @Override
        public FrequencySetting withInitialDelay(long initDelay, TimeUnit unit) {
            return new ScheduleFrequency(this, initDelay, unit);
        }

    }

    private static class ScheduleFrequency implements FrequencySetting {

        final Init init;

        final long initDelay;

        final TimeUnit unit;

        ScheduleFrequency(Init init, long initDelay, TimeUnit unit) {
            this.init = init;
            this.initDelay = initDelay;
            this.unit = unit;
        }

        @Override
        public StartOperation atFixedRate(long period) {
            return new GoOp(() -> {
                init.scheduler.scheduleAtFixedRate(init.task, initDelay, period, unit);
                return null;
            });
        }

        @Override
        public StartOperation withFixedDelay(long delay) {
            return new GoOp(() -> {
                init.scheduler.scheduleWithFixedDelay(init.task, initDelay, delay, unit);
                return null;
            });
        }

    }

    private static class GoOp implements StartOperation {

        private final Supplier<Void> op;

        GoOp(Supplier<Void> op) {
            this.op = op;
        }

        @Override
        public void go() {
            op.get();
        }

    }

}
