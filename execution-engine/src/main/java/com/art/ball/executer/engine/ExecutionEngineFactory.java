package com.art.ball.executer.engine;

public interface ExecutionEngineFactory {

    <T> ExecutionEngine<T> createDefaultEngine(TaskReader<T> reader);

    <T> ExecutionEngine<T> createDefaultEngine(TaskReader<T> reader, int numberOfProcessingThreads);

}
