package com.art.ball.executer.engine;

import com.art.ball.common.executor.Destroyable;

public interface TaskProvider<T> extends Destroyable {

    BasicSetting start();
    
    void addTaskListener(TaskListener<T> listener);
    
    void removeTaskListener(TaskListener<T> listener);
    
}
