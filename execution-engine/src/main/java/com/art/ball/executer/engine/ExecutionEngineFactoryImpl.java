package com.art.ball.executer.engine;

import java.util.concurrent.Executors;

import org.springframework.stereotype.Component;

import com.art.ball.common.executor.AsyncDispatcher;

@Component
class ExecutionEngineFactoryImpl implements ExecutionEngineFactory {

    private static final Runtime RUNTIME = Runtime.getRuntime();

    @Override
    public <T> ExecutionEngine<T> createDefaultEngine(TaskReader<T> reader) {
        return createDefaultEngine(reader, RUNTIME.availableProcessors());
    }

    @Override
    public <T> ExecutionEngine<T> createDefaultEngine(TaskReader<T> reader, int numberOfProcessingThreads) {
        TaskProvider<T> provider = new PollingTaskProvider<>(Executors.newSingleThreadScheduledExecutor(), reader);
        AsyncDispatcher<T> dispatcher = new AsyncDispatcher<>(
                Executors.newFixedThreadPool(numberOfProcessingThreads), false);
        return new ExecutionEngine<>(provider, dispatcher);
    }
    
}
