package com.art.ball.executer.engine;

import static java.util.Objects.requireNonNull;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Supplier;

import com.art.ball.common.executor.Destroyable;
import com.art.ball.common.executor.Dispatcher;
import com.art.ball.common.executor.ShutdownOperation;

public final class ExecutionEngine<T> implements Destroyable {

    private final AtomicBoolean destroyed = new AtomicBoolean(false);
    
    private final TaskProvider<T> taskProvider;

    private final Dispatcher<T> dispatcher;

    private final TaskListener<T> taskListener;
    
    public ExecutionEngine(TaskProvider<T> taskProvider, Dispatcher<T> dispatcher) {
        this.taskProvider = requireNonNull(taskProvider, "taskProvider");
        this.dispatcher = requireNonNull(dispatcher, "dispatcher");
        this.taskListener = tasks -> tasks.forEach(dispatcher::submit);
    }

    public BasicSetting start() {
        taskProvider.addTaskListener(taskListener);
        return taskProvider.start();
    }

    @Override
    public boolean isDestroyed() {
        return destroyed.get();
    }
    
    @Override
    public ShutdownOperation destroy() {
        if(destroyed.compareAndSet(false, true)) {
            return new ShutdownOp<T>(() -> {
                taskProvider.removeTaskListener(taskListener);
                return taskProvider.destroy();
            }, dispatcher);
        } else {
            return (t, u) -> {};
        }
    }

    private static class ShutdownOp<T> implements ShutdownOperation {

        private final ShutdownOperation taskProviderShutdown;
        
        private final ShutdownOperation dispatcherShutdown;

        ShutdownOp(Supplier<ShutdownOperation> shutdownTask, Dispatcher<T> dispatcher) {
            this.taskProviderShutdown = shutdownTask.get();
            this.dispatcherShutdown = dispatcher.destroy();
        }

        @Override
        public void awaitTimeout(long timeout, TimeUnit unit) {
            taskProviderShutdown.awaitTimeout(timeout, unit);
            dispatcherShutdown.awaitTimeout(timeout, unit);
        }

    }

}
