package com.art.ball.executer.engine;

import java.util.Collection;

import com.art.ball.common.executor.Task;

public interface TaskListener<T> {

    void onTasks(Collection<Task<T>> tasks);
    
}
