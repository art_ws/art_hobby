package com.art.ball.executer.engine;

import java.util.concurrent.TimeUnit;

public interface BasicSetting extends StartOperation {

    FrequencySetting withInitialDelay(long period, TimeUnit unit);
    
}
