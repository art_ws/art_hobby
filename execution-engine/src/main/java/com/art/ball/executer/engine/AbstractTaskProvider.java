package com.art.ball.executer.engine;

import java.util.Collection;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.atomic.AtomicBoolean;

import com.art.ball.common.executor.ShutdownOperation;
import com.art.ball.common.executor.Task;

public abstract class AbstractTaskProvider<T> implements TaskProvider<T> {

    private final CopyOnWriteArrayList<TaskListener<T>> listeners = new CopyOnWriteArrayList<>();

    private final AtomicBoolean destroyed = new AtomicBoolean(false);
    
    @Override
    public void addTaskListener(TaskListener<T> listener) {
        listeners.add(listener);
    }

    @Override
    public void removeTaskListener(TaskListener<T> listener) {
        listeners.remove(listener);
    }

    public void fireTasks(Collection<Task<T>> tasks) {
        listeners.forEach(l -> l.onTasks(tasks));
    }

    @Override
    public boolean isDestroyed() {
        return destroyed.get();
    }
    
    @Override
    public ShutdownOperation destroy() {
        if(destroyed.compareAndSet(false, true)) {
            listeners.clear();
            return doDestroy();
        } else {
            return (t, u) -> {};
        }
    }
    
    protected abstract ShutdownOperation doDestroy();
    
}
