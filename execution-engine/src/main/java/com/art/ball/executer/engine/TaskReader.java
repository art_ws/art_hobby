package com.art.ball.executer.engine;

import java.util.Collection;

import com.art.ball.common.executor.Task;

public interface TaskReader<T> {

    Collection<Task<T>> read();
    
}
