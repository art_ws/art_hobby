package com.art.ball.executor.engine;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Arrays;
import java.util.Collections;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.stream.Stream;

import org.awaitility.Awaitility;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.Mockito;

import com.art.ball.common.executor.Task;
import com.art.ball.executer.engine.PollingTaskProvider;
import com.art.ball.executer.engine.TaskProvider;
import com.art.ball.executer.engine.TaskReader;

final class PollingTaskProviderTest {

    @ParameterizedTest
    @MethodSource("argTestScheduling")
    final void testAtFixedRate(TaskProvider<String> provider, TaskReader<String> reader, Task<String> task) {
        provider.start()
              .withInitialDelay(0, TimeUnit.MILLISECONDS)
              .atFixedRate(50)
              .go();
        
        Awaitility.await()
            .pollInterval(25, TimeUnit.MILLISECONDS)
            .atMost(250, TimeUnit.MILLISECONDS)
            .untilAsserted(() -> Mockito.verify(task, Mockito.times(4)));
        Mockito.verify(reader, Mockito.times(4)).read();
        
        assertFalse(provider.isDestroyed());
        provider.destroy().awaitTimeout(1, TimeUnit.SECONDS);
        assertTrue(provider.isDestroyed());
    }
    
    @ParameterizedTest
    @MethodSource("argTestScheduling")
    final void testWithFixedDelay(TaskProvider<String> provider, TaskReader<String> reader, Task<String> task) {
        provider.start()
              .withInitialDelay(0, TimeUnit.MILLISECONDS)
              .withFixedDelay(50)
              .go();
        
        Awaitility.await()
            .pollInterval(25, TimeUnit.MILLISECONDS)
            .atMost(250, TimeUnit.MILLISECONDS)
            .untilAsserted(() -> Mockito.verify(task));
        Mockito.verify(reader).read();
        
        assertFalse(provider.isDestroyed());
        provider.destroy().awaitTimeout(1, TimeUnit.SECONDS);
        assertTrue(provider.isDestroyed());
    }

    @SuppressWarnings("unchecked")
    private static Stream<Arguments> argTestScheduling() {
        Task<String> task = createSlowMockTask();
        TaskReader<String> reader = Mockito.mock(TaskReader.class);
        Mockito.when(reader.read()).thenReturn(Arrays.asList(task));
        
        TaskProvider<String> provider = new PollingTaskProvider<>(Executors.newScheduledThreadPool(1),
                        reader);
        return Stream.of(
                    Arguments.of(provider, reader, task)
                );
    }

    @SuppressWarnings("unchecked")
    @Test
    final void testDestruction() {
        Task<String> task = Mockito.mock(Task.class);
        Mockito.when(task.getId()).thenReturn("abc");
        Mockito.when(task.getParams()).thenReturn(Collections.emptyMap());
        
        TaskProvider<String> provider = new PollingTaskProvider<>(Executors.newScheduledThreadPool(1),
                (TaskReader<String>) () -> Arrays.asList(task));
        
        provider.start()
            .withInitialDelay(0, TimeUnit.MILLISECONDS)
            .atFixedRate(20)
            .go();
        
        Awaitility.await()
            .pollInterval(10, TimeUnit.MILLISECONDS)
            .atMost(50, TimeUnit.MILLISECONDS)
            .untilAsserted(() -> Mockito.verify(task, Mockito.times(2)));

        assertFalse(provider.isDestroyed());
        provider.destroy().awaitTimeout(1, TimeUnit.SECONDS);
        assertTrue(provider.isDestroyed());
        
        Awaitility.await()
            .pollInterval(10, TimeUnit.MILLISECONDS)
            .atMost(50, TimeUnit.MILLISECONDS)
            .untilAsserted(() -> Mockito.verify(task, Mockito.times(2)));
    }

    @SuppressWarnings("unchecked")
    static Task<String> createSlowMockTask() {
        Task<String> task = Mockito.mock(Task.class);
        Mockito.when(task.getId()).thenReturn("abc");
        Mockito.when(task.getParams()).thenReturn(Collections.emptyMap());
        Mockito.doAnswer(invocation -> {
                    try {
                        Thread.sleep(200);
                    } catch(InterruptedException e) {
                        Thread.currentThread().interrupt();
                    }
                    return null;
                })
            .when(task)
            .run();
        return task;
    }
    
}
