package com.art.ball.executor.engine;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import com.art.ball.common.executor.Task;
import com.art.ball.executer.engine.AbstractTaskProvider;
import com.art.ball.executer.engine.TaskListener;

final class AbstractTaskProviderTest {

    @SuppressWarnings("unchecked")
    @Test
    final void testListeners() {
        AbstractTaskProvider<String> taskProvider = Mockito.mock(AbstractTaskProvider.class,
                Mockito.withSettings().useConstructor().defaultAnswer(Mockito.CALLS_REAL_METHODS));
        
        Task<String> task = Mockito.mock(Task.class);
        Collection<Task<String>> tasks = Arrays.asList(task);
        
        TaskListener<String> listener = Mockito.mock(TaskListener.class);
        
        taskProvider.addTaskListener(listener);
        taskProvider.fireTasks(tasks);
        Mockito.verify(listener).onTasks(Mockito.eq(tasks));
        taskProvider.fireTasks(tasks);
        Mockito.verify(listener, Mockito.times(2)).onTasks(Mockito.eq(tasks));
        
        taskProvider.removeTaskListener(listener);
        taskProvider.fireTasks(tasks);
        Mockito.verify(listener, Mockito.times(2)).onTasks(Mockito.eq(tasks));
    }
    
    @SuppressWarnings("unchecked")
    @Test
    final void testDestroy() {
        AbstractTaskProvider<String> taskProvider = Mockito.mock(AbstractTaskProvider.class,
                Mockito.withSettings().useConstructor().defaultAnswer(Mockito.CALLS_REAL_METHODS));
        
        TaskListener<String> listener = Mockito.mock(TaskListener.class);
        
        Collection<Task<String>> tasks = Collections.emptyList();
        taskProvider.addTaskListener(listener);
        taskProvider.fireTasks(tasks);
        Mockito.verify(listener).onTasks(Mockito.eq(tasks));
        
        assertFalse(taskProvider.isDestroyed());
        taskProvider.destroy();
        assertTrue(taskProvider.isDestroyed());
        
        taskProvider.fireTasks(tasks);
        Mockito.verify(listener).onTasks(Mockito.eq(tasks));
    }

}
