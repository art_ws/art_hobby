package com.art.ball.executor.engine;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Arrays;
import java.util.Collections;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.stream.Stream;

import org.awaitility.Awaitility;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.Mockito;

import com.art.ball.common.executor.AsyncDispatcher;
import com.art.ball.common.executor.Task;
import com.art.ball.executer.engine.ExecutionEngine;
import com.art.ball.executer.engine.PollingTaskProvider;
import com.art.ball.executer.engine.TaskProvider;
import com.art.ball.executer.engine.TaskReader;

final class ExecutionEngineTest {

    @ParameterizedTest
    @MethodSource("argTestScheduling")
    final void testAtFixedRate(ExecutionEngine<String> engine, Task<String> task) {
        engine.start()
              .withInitialDelay(0, TimeUnit.MILLISECONDS)
              .atFixedRate(50)
              .go();
        
        Awaitility.await()
            .pollInterval(25, TimeUnit.MILLISECONDS)
            .atMost(250, TimeUnit.MILLISECONDS)
            .untilAsserted(() -> Mockito.verify(task, Mockito.times(4)));
        
        engine.destroy().awaitTimeout(1, TimeUnit.SECONDS);
        assertTrue(engine.isDestroyed());
    }
    
    @ParameterizedTest
    @MethodSource("argTestScheduling")
    final void testWithFixedDelay(ExecutionEngine<String> engine, Task<String> task) {
        engine.start()
              .withInitialDelay(0, TimeUnit.MILLISECONDS)
              .withFixedDelay(50)
              .go();
        
        Awaitility.await()
            .pollInterval(25, TimeUnit.MILLISECONDS)
            .atMost(250, TimeUnit.MILLISECONDS)
            .untilAsserted(() -> Mockito.verify(task));
        
        engine.destroy().awaitTimeout(1, TimeUnit.SECONDS);
        assertTrue(engine.isDestroyed());
    }

    private static Stream<Arguments> argTestScheduling() {
        Task<String> task = PollingTaskProviderTest.createSlowMockTask();
        
        TaskProvider<String> provider = new PollingTaskProvider<>(Executors.newScheduledThreadPool(1),
                (TaskReader<String>) () -> Arrays.asList(task));
        ExecutionEngine<String> engine = new ExecutionEngine<>(provider,
                        new AsyncDispatcher<>(Executors.newFixedThreadPool(2), false));
        return Stream.of(
                    Arguments.of(engine, task)
                );
    }
    
    @SuppressWarnings("unchecked")
    @Test
    final void testDestruction() {
        Task<String> task = Mockito.mock(Task.class);
        Mockito.when(task.getId()).thenReturn("abc");
        Mockito.when(task.getParams()).thenReturn(Collections.emptyMap());
        
        TaskProvider<String> provider = new PollingTaskProvider<>(Executors.newScheduledThreadPool(1),
                (TaskReader<String>) () -> Arrays.asList(task));
        ExecutionEngine<String> engine = new ExecutionEngine<>(provider,
                new AsyncDispatcher<>(Executors.newFixedThreadPool(2), false));
        
        engine.start()
            .withInitialDelay(0, TimeUnit.MILLISECONDS)
            .atFixedRate(20)
            .go();
        
        Awaitility.await()
            .pollInterval(10, TimeUnit.MILLISECONDS)
            .atMost(50, TimeUnit.MILLISECONDS)
            .untilAsserted(() -> Mockito.verify(task, Mockito.times(2)));

        assertFalse(engine.isDestroyed());
        engine.destroy().awaitTimeout(1, TimeUnit.SECONDS);
        assertTrue(engine.isDestroyed());
        
        Awaitility.await()
            .pollInterval(10, TimeUnit.MILLISECONDS)
            .atMost(50, TimeUnit.MILLISECONDS)
            .untilAsserted(() -> Mockito.verify(task, Mockito.times(2)));
    }
    
}
